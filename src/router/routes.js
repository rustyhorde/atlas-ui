export default [
    {
        'name': 'home',
        'path': '/'
    },
    {
        'name': 'login',
        'path': '/login'
    },
    {
        'name': 'register',
        'path': '/register'
    },
    {
        'name': 'tfa',
        'path': '/tfa'
    }
]
