import superagent from 'superagent'
import {actions} from 'redux-router5'
import jwtDecode from 'jwt-decode'

export const ATLAS_ACCESS_TOKEN = 'atlas_tk'
const LOGIN_FAILURE = '@@atlas/LOGIN_FAILURE'
const LOGIN_SUCCESS = '@@atlas/LOGIN_SUCCESS'
const TFA_REQUIRED = '@@atlas/TFA_REQUIRED'
const VALID_TOKEN = '@@atlas/VALID_TOKEN'
const INVALID_TOKEN = '@@atlas/INVALID_TOKEN'

export function authenticate(values, props) {
    return function (dispatch) {
        let url = ''
        if (__DEV__) {
            url += 'http://localhost/'
        }
        url += 'api/v1/auths'
        superagent.post(url)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .withCredentials()
            .send(JSON.stringify({
                'username': values.username,
                'password': values.password
            })).then((response) => {
                const { redirectTo } = props
                const { token } = response.body

                try {
                    let payload = jwtDecode(token)

                    if (payload.tfa === true) {
                        dispatch(tfaRequired(response))
                        dispatch(actions.navigateTo('tfa', {redirectTo: redirectTo}))
                    } else {
                        dispatch(loginSuccess(response, payload))
                        dispatch(actions.navigateTo(redirectTo))
                    }
                } catch (e) {
                    dispatch(loginFailure({ 'response': {
                      'statusCode': 403,
                      'statusText': 'Invalid token'
                    }}))
                }
            }, (err) => {
                dispatch(loginFailure(err.response))
            })
    }
}

export function validateToken(values, props) {
    return function (dispatch) {
        let url = ''
        if (__DEV__) {
            url += 'http://localhost/'
        }
        url += 'api/v1/auths/' + props.id
        superagent.patch(url)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .set('Authorization', 'Bearer ' + props.payload)
            .withCredentials()
            .send(JSON.stringify({
                'totp': parseInt(values.token),
                'action': { 'type': 'CheckTotp' }
            })).then((response) => {
                const { redirectTo } = props
                const { token } = response.body

                try {
                    let payload = jwtDecode(token)

                    dispatch(loginSuccess(response, payload))
                    dispatch(actions.navigateTo(redirectTo))
                } catch (e) {
                    dispatch(loginFailure({ 'response': {
                      'statusCode': 403,
                      'statusText': 'Invalid token'
                    }}))
                }
            }, (err) => {
                dispatch(loginFailure(err.response))
            })
    }
}

function loginFailure (response) {
    sessionStorage.removeItem(ATLAS_ACCESS_TOKEN)
    return {
        'type': LOGIN_FAILURE,
        'status': response.statusCode,
        'message': response.statusText
    }
}

function loginSuccess (response, payload) {
    sessionStorage.setItem(ATLAS_ACCESS_TOKEN, response.body.token)
    return {
        'type': LOGIN_SUCCESS,
        'status': response.statusCode,
        'message': response.statusText,
        'payload': payload,
        'id': response.body.id
    }
}

function tfaRequired(response) {
    return {
        'type': TFA_REQUIRED,
        'payload': response.body.token,
        'id': response.body.id
    }
}

export function validToken() {
    return {
        'type': VALID_TOKEN,
        'authenticated': true
    }
}

export function invalidToken() {
    sessionStorage.removeItem(ATLAS_ACCESS_TOKEN)
    return {
        'type': INVALID_TOKEN,
        'authenticated': false
    }
}

const ACTION_HANDLERS = {
    [LOGIN_FAILURE]: (state, action) => { return {
        ...state,
        'status': action.status,
        'message': action.message
    }},
    [LOGIN_SUCCESS]: (state, { status, message, payload, id }) => { return {
        ...state,
        'status': status,
        'message': message,
        'payload': payload,
        'id': id
    }},
    [TFA_REQUIRED]: (state, { payload, id }) => { return {
        ...state,
        'payload': payload,
        'id': id
    }},
    [VALID_TOKEN]: (state, { authenticated }) => { return {
        ...state,
        authenticated: authenticated
    }},
    [INVALID_TOKEN]: (state, { authenticated }) => { return {
        ...state,
        authenticated: authenticated
    }}
}

// ----------------------------------------------------------------------
// Reducer
// ----------------------------------------------------------------------
const initialState = {
    'status': 0,
    'message': '',
    'payload': {},
    'id': 0,
    'totp': '',
    'authenticated': false
}

export default function authsReducer (state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]
    return handler ? handler(state, action) : state
}
