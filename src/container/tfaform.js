import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {change, Field, reduxForm, untouch} from 'redux-form'
import {Alert, Col, ControlLabel, FormControl, FormGroup} from 'react-bootstrap'
import {validateToken} from '../actions/auths'
import TwoFactorAuthFormButton from '../container/tfaformbutton'

const tokenProps = {
    'id': 'tfa-token',
    'label': 'Authenticator Token',
    'type': 'text',
    'autoFocus': true
}

const isNumeric = (str) => {
    var code, i, len
    var result = true

    for (i = 0, len = str.length; i < len; i++) {
      code = str.charCodeAt(i)
      if (code > 47 && code < 58) {// numeric (0-9)
          continue
      } else {
          result = false
          break
      }
    }

    return result
}

const validate = values => {
    const errors = {}

    if (!values.token) {
        errors.token = 'Required'
    } else if (!isNumeric(values.token)) {
        errors.token = 'Token must only contain numbers'
    } else if (values.token.length !== 6) {
        errors.token = 'Token must be 6 digits'
    }

    return errors
}

const warn = () => {
    const warnings = {}

    return warnings
}

class TwoFactorAuthForm extends React.Component {
    static propTypes = {
        'handleSubmit': PropTypes.func.isRequired,
        'dispatch': PropTypes.func.isRequired
    }

    tokenAlert = () => {
        const { dispatch } = this.props
        dispatch(change('tfa', 'token', ''))
        dispatch(untouch('tfa', 'token'))
    }

    renderField = ({dismiss, input, id, label, type, 'meta': {touched, error, warning}}) => {
        let vs

        if (touched && error) {
            vs = 'error'
        } else if (touched && warning && !error) {
            vs = 'warning'
        } else if (touched) {
            vs = 'success'
        }

        return (
            <div>
            <FormGroup controlId={id} validationState={vs}>
                <Col componentClass={ControlLabel} md={2}>
                    {label}
                </Col>
                <Col md={10}>
                    <FormControl type={type} {...input} />
                    <FormControl.Feedback />
                </Col>
            </FormGroup>
            {
                (touched && error) && <FormGroup>
                    <Col md={10} mdOffset={2}>
                        <Alert bsStyle='danger' closeLabel='X' onDismiss={dismiss}>
                            {error}
                        </Alert>
                    </Col>
                </FormGroup>
            }
            {
                (touched && warning && !error) && <FormGroup>
                    <Col md={10} mdOffset={2}>
                        <Alert bsStyle='warning' closeLabel='X' onDismiss={dismiss}>
                            {warning}
                        </Alert>
                    </Col>
                </FormGroup>
            }
            </div>
        )
    }

    render () {
        const {handleSubmit} = this.props

        return (
            <div>
                <form className="form-horizontal" onSubmit={handleSubmit}>
                    <Field name="token" {...tokenProps} dismiss={this.tokenAlert} component={this.renderField} />
                    <TwoFactorAuthFormButton />
                </form>
            </div>
        )
    }
}

const ReduxTwoFactorAuthForm = reduxForm({
    'form': 'tfa',
    'validate': validate,
    'warn': warn,
    'onSubmit': (values, dispatch, props) => dispatch(validateToken(values, props))
})(TwoFactorAuthForm)

const mapStateToProps = (state) => ({
    id: state.auths.id,
    payload: state.auths.payload
})

export default connect(mapStateToProps)(ReduxTwoFactorAuthForm)
