/*eslint no-console: ["error", { allow: ["warn", "error"] }] */
import React from 'react'
import PropTypes from 'prop-types'
import {change, Field, reduxForm, untouch} from 'redux-form'
import {Alert, Col, ControlLabel, FormControl, FormGroup} from 'react-bootstrap'
import {authenticate} from '../actions/auths'
import LoginFormButton from '../container/loginformbutton'

const usernameProps = {
    'id': 'login-username',
    'label': 'Username',
    'type': 'text',
    'autoFocus': true
}

const passwordProps = {
    'id': 'login-password',
    'label': 'Password',
    'type': 'password'
}

const isAlphaNumeric = (str) => {
    var code, i, len
    var result = true

    for (i = 0, len = str.length; i < len; i++) {
      code = str.charCodeAt(i)
      if ((code > 47 && code < 58) ||  // numeric (0-9)
          (code > 64 && code < 91) ||  // upper alpha (A-Z)
          (code == 95) ||              // underscore (_)
          (code > 96 && code < 123)) { // lower alpha (a-z)
          continue
      } else {
          result = false
          break
      }
    }

    return result
}

const validate = values => {
    const errors = {}

    if (!values.username) {
        errors.username = 'Required'
    } else if (!isAlphaNumeric(values.username)) {
        errors.username = 'Username must only contain letters, numbers, or underscore'
    } else if (values.username.length > 40) {
        errors.username = 'Username must be 40 characters or less'
    }

    if (!values.password) {
        errors.password = 'Required'
    } else if (values.password.length < 4 || values.password.length > 255) {
        errors.password = 'Password must be more between 4 and 255 characters'
    }

    return errors
}

const warn = values => {
    const warnings = {}

    if (values.password && values.password.length < 6) {
        warnings.password = 'Your password is less than 6 characters.'
    }

    return warnings
}

class LoginForm extends React.Component {
    static propTypes = {
        'handleSubmit': PropTypes.func.isRequired,
        'dispatch': PropTypes.func.isRequired
    }

    usernameAlert = () => {
        const { dispatch } = this.props
        dispatch(change('login', 'username', ''))
        dispatch(untouch('login', 'username'))
    }

    passwordAlert = () => {
        const { dispatch } = this.props
        dispatch(change('login', 'password', ''))
        dispatch(untouch('login', 'password'))
    }

    renderField = ({dismiss, input, id, label, type, 'meta': {touched, error, warning}}) => {
        let vs

        if (touched && error) {
            vs = 'error'
        } else if (touched && warning && !error) {
            vs = 'warning'
        } else if (touched) {
            vs = 'success'
        }

        return (
            <div>
            <FormGroup controlId={id} validationState={vs}>
                <Col componentClass={ControlLabel} md={2}>
                    {label}
                </Col>
                <Col md={10}>
                    <FormControl type={type} {...input} />
                    <FormControl.Feedback />
                </Col>
            </FormGroup>
            {
                (touched && error) && <FormGroup>
                    <Col md={10} mdOffset={2}>
                        <Alert bsStyle='danger' closeLabel='X' onDismiss={dismiss}>
                            {error}
                        </Alert>
                    </Col>
                </FormGroup>
            }
            {
                (touched && warning && !error) && <FormGroup>
                    <Col md={10} mdOffset={2}>
                        <Alert bsStyle='warning' closeLabel='X' onDismiss={dismiss}>
                            {warning}
                        </Alert>
                    </Col>
                </FormGroup>
            }
            </div>
        )
    }

    render () {
        const {handleSubmit} = this.props

        return (
            <div>
                <form className="form-horizontal" onSubmit={handleSubmit}>
                    <Field name="username" {...usernameProps} dismiss={this.usernameAlert} component={this.renderField} />
                    <Field name="password" {...passwordProps} dismiss={this.passwordAlert} component={this.renderField} />
                    <LoginFormButton />
                </form>
            </div>
        )
    }
}

export default reduxForm({
    'form': 'login',
    'validate': validate,
    'warn': warn,
    'onSubmit': (values, dispatch, props) => dispatch(authenticate(values, props))
})(LoginForm)
