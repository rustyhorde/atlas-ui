import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {submit} from 'redux-form'
import {Button, Col, FormGroup} from 'react-bootstrap'

class TwoFactorAuthButton extends Component {
    static propTypes = {'formActions': PropTypes.object.isRequired}

    render () {
        const {'formActions': {submit}} = this.props

        return (
            <FormGroup>
                <Col md={2} mdOffset={10}>
                    <Button block bsStyle="primary" onClick={() => submit('tfa')}>
                        Verify
                    </Button>
                </Col>
            </FormGroup>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({'formActions': bindActionCreators({submit}, dispatch)})

export default connect(null, mapDispatchToProps)(TwoFactorAuthButton)