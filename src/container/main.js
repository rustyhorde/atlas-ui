import React, {createElement} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {routeNodeSelector} from 'redux-router5'
import secured from './secured'
import Home from '../presentation/home'
import Login from '../presentation/login'
import Register from '../presentation/register'
import TwoFactorAuth from '../presentation/tfa'

const NotFound = () => <div><h1>404: Not Found!</h1></div>

const components = {
    'home': secured(Home),
    'login': Login,
    'register': Register,
    'tfa': TwoFactorAuth
}

function Main (props) {
    const {route} = props
    const segment = route ? route.name.split('.')[0] : undefined
    return createElement(components[segment] || NotFound, route.params)
}

Main.propTypes = {'route': PropTypes.object, 'params': PropTypes.array}

export default connect(() => routeNodeSelector(''))(Main)
