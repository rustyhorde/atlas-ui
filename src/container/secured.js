import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {actions} from 'redux-router5'
import jwtDecode from 'jwt-decode'
import * as AuthsActions from '../actions/auths'
import { ATLAS_ACCESS_TOKEN } from '../actions/auths'

export function secured(Component, redirectTo = 'home') {
    class SecuredComponent extends React.Component {
        static propTypes = {
            'payload': PropTypes.object,
            'authenticated': PropTypes.bool,
            'routerActions': PropTypes.object.isRequired,
            'authsActions': PropTypes.object.isRequired
        }
        static contextTypes = {
            'router': PropTypes.object.isRequired
        }

        constructor (props, context) {
            super(props)
            this.router = context.router
        }

        checkToken = (props) => {
            const { routerActions: { navigateTo } } = props
            const { authsActions: { invalidToken, validToken } } = props

            const token = sessionStorage.getItem(ATLAS_ACCESS_TOKEN)

            if (!token) {
                navigateTo('login', {redirectTo: redirectTo})
            } else {
                try {
                    const payload = jwtDecode(token)
                    const { iss, exp, tfa } = payload
                    const now = Math.floor(Date.now() / 1000)

                    if (iss !== 'atlas-api 0.1.0') {
                        invalidToken()
                        navigateTo('login', {redirectTo: redirectTo})
                    } else if (now > exp) {
                        invalidToken()
                        navigateTo('login', {redirectTo: redirectTo})
                    } else {
                        if (tfa === true) {
                            navigateTo('tfa', {redirectTo: redirectTo})
                        } else {
                            validToken()
                        }
                    }
                } catch (e) {
                    invalidToken()
                    navigateTo('login', {redirectTo: redirectTo})
                }
            }
        }

        componentWillMount = () => {
            this.checkToken(this.props)
        }

        componentWillReceiveProps = (nextProps) => {
            this.checkToken(nextProps)
        }

        render () {
            const { authenticated } = this.props
            const securedComponent = authenticated === true
            ? <Component {...this.props} />
            : null

            return (
                <div>
                    {securedComponent}
                </div>
            )
        }
    }

    const mapStateToProps = (state) => ({
        payload: state.auths.payload,
        authenticated: state.auths.authenticated
    })

    const mapDispatchToProps = (dispatch) => {
        return {
            routerActions: bindActionCreators({'navigateTo': actions.navigateTo}, dispatch),
            authsActions: bindActionCreators(AuthsActions, dispatch)
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(SecuredComponent)
  }

  export default secured