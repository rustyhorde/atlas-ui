import {applyMiddleware, compose, createStore} from 'redux'
import thunk from 'redux-thunk'
import {router5Middleware} from 'redux-router5'
import rootReducer from '../reducers'
import DevTools from '../container/devtools'
import createHistory from 'history/createBrowserHistory'
import {createLogger} from 'redux-logger'

export const history = createHistory()

export default function configureStore (router, initialState = {}) {
    const store = createStore(rootReducer, initialState, compose(
        applyMiddleware(thunk, router5Middleware(router), createLogger()),
        DevTools.instrument()
    ))

    if (module.hot) {
        module.hot.accept('../reducers', () => store.replaceReducer(require('../reducers')))
    }

    return store
}
