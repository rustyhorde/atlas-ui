import {applyMiddleware, compose, createStore} from 'redux'
import createHistory from 'history/createBrowserHistory'
import {createLogger} from 'redux-logger'
import rootReducer from '../reducers'
import {router5Middleware} from 'redux-router5'
import thunk from 'redux-thunk'

export const history = createHistory()

export default function configureStore (router, initialState = {}) {

    const store = createStore(rootReducer, initialState, compose(applyMiddleware(router5Middleware(router), thunk, createLogger())))

    return store

}
