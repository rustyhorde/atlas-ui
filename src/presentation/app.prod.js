import React from 'react'
import Header from './header'
import Main from '../container/main'
import {Col, Grid, Row} from 'react-bootstrap'

class App extends React.Component {

    render () {

        return (
            <Grid>
                <Row>
                    <Header />
                </Row>
                <Row>
                    <Col>
                        <Main />
                    </Col>
                </Row>
            </Grid>
        )

    }

}

export default App
