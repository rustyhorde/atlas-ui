import React from 'react'
import Header from './header'
import Main from '../container/main'
import DevTools from '../container/devtools'
import {Col, Grid, Row} from 'react-bootstrap'

class App extends React.Component {

    render () {

        return (
            <Grid>
                <Row>
                    <Header />
                </Row>
                <Row>
                    <Col>
                        <Main />
                    </Col>
                </Row>
                <DevTools />
            </Grid>
        )

    }

}

export default App
