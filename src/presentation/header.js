import React from 'react'
import PropTypes from 'prop-types'
import {Button, ButtonToolbar, Col, Jumbotron, Row} from 'react-bootstrap'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {actions} from 'redux-router5'

class Header extends React.Component {

    static propTypes = {
        'navigateTo': PropTypes.func.isRequired,
        'route': PropTypes.object.isRequired
    }

    static contextTypes = {'router': PropTypes.object.isRequired}

    constructor (props, context) {
        super(props)
        this.router = context.router
    }

    onHome = (e) => {
        const {navigateTo} = this.props
        e.preventDefault()
        navigateTo('home')
    }

    onLogin = (e) => {
        const {navigateTo} = this.props
        e.preventDefault()
        navigateTo('login', {redirectTo: 'home'})
    }

    onRegister = (e) => {
        const {navigateTo} = this.props
        e.preventDefault()
        navigateTo('register', {redirectTo: 'home'})
    }

    render () {
        return (
            <Jumbotron>
                <Row>
                    <Col
                        xs={4}
                        xsOffset={4}
                    >
                        <h1
                            className="text-center"
                            onClick={this.onHome}
                        >Atlas
                        </h1>
                    </Col>
                </Row>
                <Row>
                    <Col
                        lg={4}
                        lgOffset={8}
                    >
                        <ButtonToolbar>
                            <Button
                                bsStyle="primary"
                                className="pull-right"
                                onClick={this.onLogin}
                            >Login
                            </Button>
                            <Button
                                className="pull-right"
                                onClick={this.onRegister}
                            >Register
                            </Button>
                        </ButtonToolbar>
                    </Col>
                </Row>
            </Jumbotron>
        )

    }

}

function mapStateToProps (state) {
    return {'route': state.router.route}
}

function mapDispatchToProps (dispatch) {
    return bindActionCreators({'navigateTo': actions.navigateTo}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
