import React, { Component } from 'react'
import {Panel} from 'react-bootstrap'
import TwoFactorAuthForm from '../container/tfaform'

class TwoFactorAuth extends Component {
    render () {
        return (
            <Panel>
                <h2>Two Factor Authentication</h2>
                <TwoFactorAuthForm { ...this.props } />
            </Panel>
        )
    }
}

export default TwoFactorAuth
