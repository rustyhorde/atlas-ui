import React, {Component} from 'react'
import {Panel} from 'react-bootstrap'
import LoginForm from '../container/loginform'

class Login extends Component {
    render () {
        return (
            <Panel>
                <h2>Login</h2>
                <LoginForm { ...this.props } />
            </Panel>
        )
    }
}

export default Login
