import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {RouterProvider} from 'react-router5'
import configureStore from './store'
import createRouter from './router/create'
import App from './presentation/app.dev'
import '../styles/core.scss'

const container = document.querySelector('.container')
const router = createRouter()
const store = configureStore(router)

const wrappedApp = <Provider store={store}>
    <RouterProvider router= {router}>
        <App />
    </RouterProvider>
</Provider>

router.start(() => {

    ReactDOM.render(wrappedApp, container)

})
