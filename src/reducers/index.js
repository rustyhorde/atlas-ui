import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form'
import {router5Reducer} from 'redux-router5'
import authsReducer from '../actions/auths'

const rootReducer = combineReducers({
    'auths': authsReducer,
    'form': formReducer,
    'router': router5Reducer
})

export default rootReducer
