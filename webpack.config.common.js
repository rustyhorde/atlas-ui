const webpack = require('webpack');
const path = require('path');
const rootPublic = path.resolve('./assets')
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const _debug = require('debug');

const debug = _debug('app:webpack:config')
debug('Create configuration.')

const config = {
    env: process.env.NODE_ENV || 'development',
}

config.globals = {
    'process.env'  : {
      'NODE_ENV' : JSON.stringify(config.env)
    },
    'NODE_ENV'     : config.env,
    '__DEV__'      : config.env === 'development',
    '__PROD__'     : config.env === 'production',
    '__STAGE__'    : config.env === 'stage',
    '__BASENAME__' : JSON.stringify(process.env.BASENAME || '')
}

debug('Globals', config.globals)
module.exports = {

    entry: [
        './src/index.js'
    ],

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-0']
                }
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: `css-loader?sourceMap&root=${rootPublic}&-minimize`
                }, {
                    loader: `sass-loader?sourceMap&root=${rootPublic}`
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                enforce: 'pre',
                loader: 'eslint-loader',
                options: {
                    emitWarning: true,
                }
            },
            {
                test: /\.(png|jpg|svg|woff|woff2)?(\?v=\d+.\d+.\d+)?$/,
                loader: 'url-loader?limit=8192',
            },
        ]
    },

    resolve: {
        extensions: ['.js', '.jsx']
    },

    plugins: [
        new webpack.DefinePlugin(config.globals),
        new CleanWebpackPlugin(['dist'], {
            root: __dirname,
            verbose: true,
            dry: false
        }),
        new CopyWebpackPlugin([
            { from: 'src/index.html', to: 'index.html'}
        ]),
    ]

};